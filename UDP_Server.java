import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UDPServer {
	private static DatagramSocket aSocket;
	private static  HashMap<String,String> uList = new HashMap<String,String>();
	
	public static void register(String msg){
		uList.put(msg,msg);
	}
	
	public static void unregister(String msg){
		uList.remove(msg,msg);
	}
	
	public static void comunicate(){
		
	}
	
	public static void inform(){
		Set<Entry<String, String>> set = uList.entrySet();
	      Iterator<Entry<String, String>> iterator = set.iterator();
	      
	      while(iterator.hasNext()) {
	         HashMap.Entry mentry = (HashMap.Entry)iterator.next();
	         System.out.print("key is: "+ mentry.getKey() + " & Value is: ");
	         System.out.println(mentry.getValue());
	      }
	}
	
	public static void decode(byte[] msg){
		//sign = (string)msg;
		
		switch(msg[0]){
			case '+':
					System.out.println("Otrzymalem: "+ new String(msg));
					register(new String(msg));
				break;
			
			case '-':
					System.out.println("Otrzymalem: "+ new String(msg));
					unregister(new String(msg));
				break;
			
			case '?':
				System.out.println("Otrzymalem: "+ new String(msg));
				inform();
				break;
				
			case '!':
					System.out.println("Otrzymalem: "+ new String(msg));
				break;
				
			default: 
				System.out.println("Błędne żądanie klienta!");
				break;
		}
	}
	
	private static void loop() throws IOException{
		
			aSocket = new DatagramSocket(9876);
			byte[] buffer = new byte[1024];
			byte[] message = new byte[1024];
			
			while(true){				
				DatagramPacket request = new DatagramPacket(buffer, buffer.length);
				aSocket.receive(request);
				message = request.getData(); 
				decode(message);
				
			}						
	}
	
	public static void main(String[] args){
		try{
			loop();
			
		}
		
		catch (SocketException ex){
			Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE , null , ex);
		}
		catch (IOException ex) {
			Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE , null , ex);
		}
	}		
}